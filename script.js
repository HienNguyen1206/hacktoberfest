const quizData = [
  {
    question:
      "Trước nửa đêm là bao nhiêu phút nếu trước đó 32 phút thời gian này gấp 3 lần số phút sau 22 giờ?",
    a: "18 phút",
    b: "22 phút",
    c: "24 phút",
    d: "20 phút",
    correct: "b",
  },
  {
    question: "Ký tự tiếp theo trong dãy sau đây là ký tự nào: A…C…F…J…O…?",
    a: "S",
    b: "T",
    c: "U",
    d: "V",
    correct: "c",
  },
  {
    question:
      "Hiện nay A gấp 3 lần tuổi B, sau 10 năm nữa B sẽ bằng phân nửa tuổi A.Vậy hiện nay tuổi của A là bao nhiêu?",
    a: "24 tuổi",
    b: "30 tuổi",
    c: "36 tuổi",
    d: "40 tuổi",
    correct: "b",
  },
  {
    question: "Nếu 5y – 3x = 7 và 6y + 6x = 2 thì giá trị của y là:",
    a: "1",
    b: "2",
    c: "3",
    d: "4",
    correct: "a",
  },
];

const quiz = document.getElementById("quiz");
const answerEls = document.querySelectorAll(".answer");
const questionEl = document.getElementById("question");
const a_text = document.getElementById("a_text");
const b_text = document.getElementById("b_text");
const c_text = document.getElementById("c_text");
const d_text = document.getElementById("d_text");
const submitBtn = document.getElementById("submit");

let currentQuiz = 0;
let score = 0;

loadQuiz();

function loadQuiz() {
  deselectAnswers();

  const currentQuizData = quizData[currentQuiz];

  questionEl.innerText = currentQuizData.question;
  a_text.innerText = currentQuizData.a;
  b_text.innerText = currentQuizData.b;
  c_text.innerText = currentQuizData.c;
  d_text.innerText = currentQuizData.d;
}

function getSelected() {
  let answer = undefined;

  answerEls.forEach((answerEl) => {
    if (answerEl.checked) {
      answer = answerEl.id;
    }
  });

  return answer;
}

function deselectAnswers() {
  answerEls.forEach((answerEl) => {
    answerEl.checked = false;
  });
}

submitBtn.addEventListener("click", () => {
  // check to see the answer
  const answer = getSelected();

  if (answer) {
    if (answer === quizData[currentQuiz].correct) {
      score++;
    }

    currentQuiz++;
    if (currentQuiz < quizData.length) {
      loadQuiz();
    } else {
      quiz.innerHTML = `
                <h2>Số câu đúng ${score}/${quizData.length}.</h2>
                
                <button onclick="location.reload()">Reload</button>
            `;
    }
  }
});
